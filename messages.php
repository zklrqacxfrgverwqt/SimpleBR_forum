<?php
// Policy to not store session cache on the client side
session_cache_limiter('nocache');

// Start the session
session_start();

// Variable to store error or success messages
$error_message = "";

// Database file name
$dbFile = 'master.db';

// Create new SQLite3 connection or open existing database
$db = new SQLite3($dbFile);

// Check whether the connection was established successfully
if (!$db) {
    die("Unable to create or open database.");
}

// If the user is logged in, set $username to the session username
if (isset($_SESSION["username"]) && $_SESSION["username"] !== "") {
    $username = $_SESSION["username"];
} else {
    $username = "";
}


// Function to clean and validate user input ///////////////////////////////////////////////////////////////////////////////////////////////////////

function cleanInput($input) {
    $input = trim($input);
    $input = stripslashes($input);
    $input = htmlspecialchars($input);
    $input = urlencode($input);
    return $input;
}


// INSERT MESSAGES /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

if ($_SERVER["REQUEST_METHOD"] === "POST") {
    if (isset($_POST["send_message"])) {
        if (isset($_SESSION["username"]) && $_SESSION["username"] !== "") {
            $user = $_SESSION["username"];
            $message = cleanInput($_POST["message"]);
            $topic_id = isset($_GET["id"]) ? intval($_GET["id"]) : 0;

// Check that the message is not empty
            if (!empty($message)) {
// Insert the message into the message table
                $insertMessageQuery = $db->prepare("INSERT INTO messages (datetime, username, message, topic_id) VALUES (:datetime, :username, :message, :topic_id)");
                $insertMessageQuery->bindValue(':datetime', date('Y-m-d H:i:s'), SQLITE3_TEXT);
                $insertMessageQuery->bindValue(':username', $user, SQLITE3_TEXT);
                $insertMessageQuery->bindValue(':message', $message, SQLITE3_TEXT);
                $insertMessageQuery->bindValue(':topic_id', $topic_id, SQLITE3_INTEGER);
                $result = $insertMessageQuery->execute();
            }
//            $url = $_SERVER['PHP_SELF'] . "?id=" . $_GET['id'] . "#footer_";
//            $url = $_SERVER['PHP_SELF'] . "?id=" . $_GET['id'];
//            header("Location: $url");
//            exit;
        }
    }


// DELETE MESSAGES /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    if (isset($_POST["delete_message"])) {
        if (isset($_SESSION["username"]) && $_SESSION["username"] !== "") {
            $user = $_SESSION["username"];
            $message_id = isset($_POST["message_id"]) ? intval($_POST["message_id"]) : 0;

// Check if the user is "boss" or "admin"
            $userStatus = $_SESSION["status"];

// Check if the user has permission to delete messages
            if ($userStatus === "boss" || $userStatus === "admin") {
// Delete the message, no matter who the author is
                $deleteMessageQuery = $db->prepare("DELETE FROM messages WHERE id = :message_id");
                $deleteMessageQuery->bindValue(':message_id', $message_id, SQLITE3_INTEGER);
                $deleteMessageQuery->execute();
            } else {
// If not "boss" or "admin", check if the user is the author of the message
                $checkAuthorQuery = $db->prepare("SELECT username FROM messages WHERE id = :message_id");
                $checkAuthorQuery->bindValue(':message_id', $message_id, SQLITE3_INTEGER);
                $resultAuthor = $checkAuthorQuery->execute();
                $author = $resultAuthor->fetchArray(SQLITE3_ASSOC);

// If the author of the message is the same as the current user, he can delete it
                if ($author['username'] === $user) {
                    $deleteMessageQuery = $db->prepare("DELETE FROM messages WHERE id = :message_id");
                    $deleteMessageQuery->bindValue(':message_id', $message_id, SQLITE3_INTEGER);
                    $deleteMessageQuery->execute();
                }
            }
        }
    }
}


// Query to get messages for the specific topic ////////////////////////////////////////////////////////////////////////////////////////////

$topic_id = isset($_GET["id"]) ? intval($_GET["id"]) : 0; // Make sure to validate and clear the topic ID input
$queryMessages = $db->prepare("SELECT id, username, datetime, message FROM messages WHERE topic_id = :topic_id");
$queryMessages->bindValue(':topic_id', $topic_id, SQLITE3_INTEGER);
$resultMessages = $queryMessages->execute();

$messages = [];
while ($row = $resultMessages->fetchArray(SQLITE3_ASSOC)) {
    $messages[] = $row;
}


// FEEDBACK /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

if (isset($_POST["like_message"]) || isset($_POST["dislike_message"])) {
    if (isset($_SESSION["username"]) && $_SESSION["username"] !== "") {
        $user = $_SESSION["username"];
        $message_id = isset($_POST["message_id"]) ? intval($_POST["message_id"]) : 0;
        $feedback_type = isset($_POST["like_message"]) ? "Like" : "Dislike";

// Get the user_id based on the username
        $getUserQuery = $db->prepare("SELECT id FROM user WHERE username = :username");
        $getUserQuery->bindValue(':username', $user, SQLITE3_TEXT);
        $userRow = $getUserQuery->execute()->fetchArray(SQLITE3_ASSOC);

        if ($userRow) {
            $user_id = $userRow['id'];

// Get the username of the user who posted the message
            $getMessageUsernameQuery = $db->prepare("SELECT username FROM messages WHERE id = :message_id");
            $getMessageUsernameQuery->bindValue(':message_id', $message_id, SQLITE3_INTEGER);
            $messageUsernameRow = $getMessageUsernameQuery->execute()->fetchArray(SQLITE3_ASSOC);

            if ($messageUsernameRow) {
                $messageUsername = $messageUsernameRow['username'];

// Check if the user has already given feedback for this message
                $checkFeedbackQuery = $db->prepare("SELECT id, feedback_type FROM message_feedback WHERE user_id = :user_id AND message_id = :message_id");
                $checkFeedbackQuery->bindValue(':user_id', $user_id, SQLITE3_INTEGER);
                $checkFeedbackQuery->bindValue(':message_id', $message_id, SQLITE3_INTEGER);
                $existingFeedback = $checkFeedbackQuery->execute()->fetchArray(SQLITE3_ASSOC);

                if (!$existingFeedback) {
// Insert feedback into the table
                    $insertFeedbackQuery = $db->prepare("INSERT INTO message_feedback (user_id, message_id, feedback_type, date) VALUES (:user_id, :message_id, :feedback_type, :date)");
                    $insertFeedbackQuery->bindValue(':user_id', $user_id, SQLITE3_INTEGER);
                    $insertFeedbackQuery->bindValue(':message_id', $message_id, SQLITE3_INTEGER);
                    $insertFeedbackQuery->bindValue(':feedback_type', $feedback_type, SQLITE3_TEXT);
                    $insertFeedbackQuery->bindValue(':date', date('Y-m-d H:i:s'), SQLITE3_TEXT);
                    $insertFeedbackQuery->execute();

// Update the user's reward
                    $updateRewardQuery = $db->prepare("UPDATE user SET reward = reward + " . ($feedback_type === "Like" ? 1 : -1) . " WHERE username = :username");
                    $updateRewardQuery->bindValue(':username', $messageUsername, SQLITE3_TEXT);
                    $updateRewardQuery->execute();
                } else {
// Handle the case when the user already provided feedback
                    $existingFeedbackType = $existingFeedback['feedback_type'];

                    if ($existingFeedbackType !== $feedback_type) {
// User changes their feedback type
                        $updateFeedbackQuery = $db->prepare("UPDATE message_feedback SET feedback_type = :feedback_type, date = :date WHERE id = :feedback_id");
                        $updateFeedbackQuery->bindValue(':feedback_id', $existingFeedback['id'], SQLITE3_INTEGER);
                        $updateFeedbackQuery->bindValue(':feedback_type', $feedback_type, SQLITE3_TEXT);
                        $updateFeedbackQuery->bindValue(':date', date('Y-m-d H:i:s'), SQLITE3_TEXT);
                        $updateFeedbackQuery->execute();

// Update the user's reward when changing feedback
                        $updateRewardQuery = $db->prepare("UPDATE user SET reward = reward + " . ($feedback_type === "Like" ? 2 : -2) . " WHERE username = :username");
                        $updateRewardQuery->bindValue(':username', $messageUsername, SQLITE3_TEXT);
                        $updateRewardQuery->execute();
                    } else {
// User removes their feedback
                        $deleteFeedbackQuery = $db->prepare("DELETE FROM message_feedback WHERE id = :feedback_id");
                        $deleteFeedbackQuery->bindValue(':feedback_id', $existingFeedback['id'], SQLITE3_INTEGER);
                        $deleteFeedbackQuery->execute();

// Update the user's reward when removing feedback
                        $updateRewardQuery = $db->prepare("UPDATE user SET reward = reward - " . ($feedback_type === "Like" ? 1 : -1) . " WHERE username = :username");
                        $updateRewardQuery->bindValue(':username', $messageUsername, SQLITE3_TEXT);
                        $updateRewardQuery->execute();
                    }
                }
            }
        }
    }
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SimpleBR Forum</title>
    <link rel="stylesheet" type="text/css" href="styles.css">
</head>

<body>
    <div class="title_fix">
        <div class="forum-global" style="text-align: left;">
            <?php
            $url = $_SERVER['PHP_SELF'] . "?id=" . $_GET['id'];
            ?>
            <a href="<?php echo $url; ?>">
                <img src="logo.jpg" alt="Ícone" width="60" height="60" style="display: inline-block; margin-left: -40px;">
                <div class="forum-title"><span>SimpleBR </span><span style="font-weight: 100;">Forum</span></div>
            </a>
            <a href="index.php" class="home">HOME</a>
            <a href="<?php echo $url; ?>" class="home">RELOAD</a>
            <br>
        </div>
        <div id="separator_message"></div>

        <?php
// Check if the 'id' parameter was passed in the URL
        if (isset($_GET['id'])) {
            $topicId = $_GET['id'];
// Connect to database
            $db = new SQLite3('master.db');
// Query to get topic name based on ID
            $queryTopicName = $db->prepare("SELECT topic_name FROM topics WHERE id = :topic_id");
            $queryTopicName->bindValue(':topic_id', $topicId, SQLITE3_INTEGER);
            $resultTopicName = $queryTopicName->execute()->fetchArray(SQLITE3_ASSOC);
//    $db->close();
            if ($resultTopicName) {
                $topicName = $resultTopicName['topic_name'];
                echo "TOPIC: " . htmlspecialchars(urldecode($topicName), ENT_QUOTES, 'UTF-8');
            }
        }
        ?>

        <?php
// Connect to the database
        $db = new SQLite3('master.db');

        if (isset($_GET['id'])) {
            $topicId = $_GET['id'];

            $queryMessageCount = $db->prepare("SELECT COUNT(*) as message_count FROM messages WHERE topic_id = :topic_id");
            $queryMessageCount->bindValue(':topic_id', $topicId, SQLITE3_INTEGER);
            $messageCount = $queryMessageCount->execute()->fetchArray(SQLITE3_ASSOC);
            echo '<div class="title_right">';
                echo 'Messages: ' . $messageCount['message_count'];
            echo '</div>';

            $queryUserCount = $db->prepare("SELECT COUNT(DISTINCT username) as user_count FROM messages WHERE topic_id = :topic_id");
            $queryUserCount->bindValue(':topic_id', $topicId, SQLITE3_INTEGER);
            $userCount = $queryUserCount->execute()->fetchArray(SQLITE3_ASSOC);
            echo '<div class="title_right">';
                echo '<div class="view_users">';
                    echo 'Participants: ' . $userCount['user_count'];
                    echo '<div class="exposed_users">';
                        $queryUserList = $db->prepare("SELECT DISTINCT username FROM messages WHERE topic_id = :topic_id");
                        $queryUserList->bindValue(':topic_id', $topicId, SQLITE3_INTEGER);
                        $userListResult = $queryUserList->execute();
                        if ($userListResult) {
                            while ($user = $userListResult->fetchArray(SQLITE3_ASSOC)) {
                                echo htmlspecialchars(urldecode($user['username']), ENT_QUOTES, 'UTF-8') . '<br>';
                            }
                        }
                    echo '</div>';
                echo '</div>';
            echo '</div>';
        }
        ?>
    </div>

    <div class="forum-global">
        <div class="message_container"><br></br><br></br>
            <div id="msg" style="<?php echo count($messages) < 10 ? 'margin-top: 4.1%;' : ''; ?>">
                <div class="msg_">
                    <?php
                    $colors = [
                        '#C5C7FA', '#D2F6F8', '#A5F1FF', '#96EBFB', '#82E6F9', '#5DD4EA', '#4DC7DE', '#3FB7CE', '#33A9C0', '#2A9EB5', '#1D92A9',
                        '#4E97A5', '#63A5B2', '#7AB3BF', '#92B3BA', '#49B0FC', '#38A4F4', '#2A93E0', '#1A83D0', '#438ABF', '#464BDC'
                    ];

                    foreach ($messages as $message):
                        $randomColor = $colors[array_rand($colors)];

                        $messageUsername = $message['username'];
                        $getUserQuery = $db->prepare("SELECT id, status FROM user WHERE username = :username");
                        $getUserQuery->bindValue(':username', $messageUsername, SQLITE3_TEXT);
                        $userRow = $getUserQuery->execute()->fetchArray(SQLITE3_ASSOC);

                        if ($userRow) {
                            $userStatus = $userRow['status'];
                        }

                        if (isset($_SESSION["username"]) && $_SESSION["username"] !== "") {
                            $user = $_SESSION["username"];
                        }
                        $getUserQuery = $db->prepare("SELECT id FROM user WHERE username = :username");
                        $getUserQuery->bindValue(':username', $user, SQLITE3_TEXT);
                        $userRow = $getUserQuery->execute()->fetchArray(SQLITE3_ASSOC);
                        if ($userRow) {
                            $user_id = $userRow['id'];
                            $message_id = isset($_POST["message_id"]) ? intval($_POST["message_id"]) : 0;
                            $checkFeedbackQuery = $db->prepare("SELECT id, feedback_type FROM message_feedback WHERE user_id = :user_id AND message_id = :message_id");
                            $checkFeedbackQuery->bindValue(':user_id', $user_id, SQLITE3_INTEGER);
                            $checkFeedbackQuery->bindValue(':message_id', $message_id, SQLITE3_INTEGER);
                            $existingFeedback = $checkFeedbackQuery->execute()->fetchArray(SQLITE3_ASSOC);
                            $existingFeedbackType = '';
                            $existingFeedback = $checkFeedbackQuery->execute()->fetchArray(SQLITE3_ASSOC);
                            if ($existingFeedback) {
                                $existingFeedbackType = $existingFeedback['feedback_type'];
                            }
                        }
                        ?>

                        <?php
                        $messageTime = strtotime($message['datetime']);
                        $currentTime = time();
                        $timeElapsed = $currentTime - $messageTime;

                        if ($timeElapsed < 60) {
                            $timeAgo = $timeElapsed . ($timeElapsed == 1 ? ' second' : ' seconds');
                        } elseif ($timeElapsed < 3600) {
                            $minutes = floor($timeElapsed / 60);
                            $timeAgo = $minutes . ($minutes == 1 ? ' minute' : ' minutes');
                        } elseif ($timeElapsed < 86400) {
                            $hours = floor($timeElapsed / 3600);
                            $timeAgo = $hours . ($hours == 1 ? ' hour' : ' hours');
                        } elseif ($timeElapsed < 604800) {
                            $days = floor($timeElapsed / 86400);
                            $timeAgo = $days . ($days == 1 ? ' day' : ' days');
                        } elseif ($timeElapsed < 2419200) {
                            $weeks = floor($timeElapsed / 604800);
                            $timeAgo = $weeks . ($weeks == 1 ? ' week' : ' weeks');
                        } else {
                            $months = floor($timeElapsed / 2419200);
                            $timeAgo = $months . ($months == 1 ? ' month' : ' months');
                        }
                        ?>

                        <div id="border_msg">
                            <div style="float: left; width: 50%; color: <?php echo $randomColor; ?>; font-size: 20px;">
                                <?php
                                $escapedMessageUsername = htmlspecialchars(urldecode($message['username']), ENT_QUOTES);
                                $escapedUserStatus = htmlspecialchars(urldecode($userStatus), ENT_QUOTES);
                                echo $escapedMessageUsername . " (" . $escapedUserStatus . ")";
                                ?>

                            </div>
                            <div style="float: right; width: 50%; color: #0EAEF8; font-size: 14px; text-align: right;">
                                <?php echo $timeAgo; ?> ago
                            </div>

                                <?php
                                $likeCount = $db->querySingle("SELECT COUNT(*) FROM message_feedback WHERE message_id = {$message['id']} AND feedback_type = 'Like'");
                                $dislikeCount = $db->querySingle("SELECT COUNT(*) FROM message_feedback WHERE message_id = {$message['id']} AND feedback_type = 'Dislike'");
                                ?>

                                <div style="clear: both; color: #ffffff; font-size: 16px; margin-bottom: -20px; max-width: 85%;"><?php echo  htmlspecialchars_decode(urldecode($message['message'])); ?></div>
                                <form method="post">
                                    <input type="hidden" name="message_id" value="<?php echo $message['id']; ?>">
                                    <button type="submit" name="like_message" value="Like" style="float: right; color: #000000; border: 0.5px solid #01FF09; background-color: #01FF09; font-size: 10px; font-weight: bold;">Like <?php echo $likeCount; ?></button>
                                </form>
                                <form method="post">
                                    <input type="hidden" name="message_id" value="<?php echo $message['id']; ?>">
                                    <button type="submit" name="dislike_message" value="Dislike" style="float: right; color: #000000; border: 0.5px solid #FFBD01; background-color: #FFBD01; font-size: 10px; font-weight: bold;">Dislike <?php echo $dislikeCount; ?></button>
                                </form>

                                <?php
                                if (isset($_SESSION["username"]) && $_SESSION["username"] !== "") {
                                    $db = new SQLite3('master.db');
// Query the database to get the user's status based on their username (assuming $_SESSION['username'] contains the username)
                                    $queryStatus = $db->prepare("SELECT status FROM user WHERE username = :username");
                                    $queryStatus->bindValue(':username', $_SESSION['username'], SQLITE3_TEXT);
                                    $resultStatus = $queryStatus->execute();
                                    if ($rowStatus = $resultStatus->fetchArray(SQLITE3_ASSOC)) {
                                        $userStatus = $rowStatus['status'];
                                    }
                                }
                                if (isset($_SESSION["username"]) && $_SESSION["username"] !== "" && $userStatus === "boss" || $userStatus === "admin" or $_SESSION["username"] === $message['username']) : ?>
                                    <form method="post">
                                        <input type="hidden" name="message_id" value="<?php echo $message['id']; ?>">
                                        <button type="submit" name="delete_message" style="float: right; color: #000000; border: 0.5px solid red; background-color: red; font-size: 10px; font-weight: bold;">Delete</button>
                                    </form>
                                <?php endif; ?>
                        </div>
                        <br>
                        <div id="separator_message"></div>
                        </br>

                    <?php endforeach; ?>

                    <?php
                    $url = $_SERVER['PHP_SELF'] . "?id=" . $_GET['id'];
                    if (isset($_SESSION["username"]) && $_SESSION["username"] !== "") {
                        echo '<form method="post" action="' . $url . '">';
                        echo '<input type="message" id="message" name="message" placeholder="Message (maxlength=240)" maxlength="240" autofocus>';
                        echo '<input type="submit" name="send_message" value="Send">';
                        echo '<a href="' . $url . '" id="footer_section" style="margin-right: 5px; margin-left: 5px;">Reload</a>';
                        echo '<a href="#top" id="footer_section" style="margin-right: 5px; margin-left: 5px;">Up</a>';
                        echo '</form>';
                    }
                    ?>

                    <footer id="footer_"></footer>

                </div>
            </div>
        </div>
    </div>
</body>
</html>
