# About
Chat forum, with users status and penalty for feedback. Absolutely 0 JavaScript, focused on privacy and zero kyc

## Required Programs
php8.2  
php8.2-sqlite3  
php8.2-memcached

## How to start:
Make sure you're inside the root directory:
```bash
php8.2 -S 127.0.0.1:9000
```

## How to access:
Make sure you're in a web browser:
```bash
http://127.0.0.1:9000/index.php
```
  
### Screenshots
| home | messages |
|------------------------------------------------------|---|
| ![ Photo of stream ]( home.png  "title" ) | ![ Photo of stream ]( messages.png  "title" ) |

