<?php
// Policy to not store session cache on the client side
session_cache_limiter('nocache');

// Start the session
session_start();

// Variable to store error or success messages
$error_message = "";

// Database file name
$dbFile = 'master.db';

// Create new SQLite3 connection or open existing database
$db = new SQLite3($dbFile);

// Check whether the connection was established successfully
if (!$db) {
    die("Unable to create or open database.");
}

// If the user is logged in, set $username to the session username
if (isset($_SESSION["username"]) && $_SESSION["username"] !== "") {
    $username = $_SESSION["username"];
} else {
    $username = "";
}

/*
banned
disturbing
tourist
junior
pleno
senior
major
bot
lordy
admin
boss
*/

$queryUser = "CREATE TABLE IF NOT EXISTS user (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    username TEXT NOT NULL,
    hash_password TEXT NOT NULL,
    date DATETIME NOT NULL,
    historic TEXT,
    status TEXT,
    reward INTEGER DEFAULT 0
)";
$db->exec($queryUser);

$queryTopic = "CREATE TABLE IF NOT EXISTS topics (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    datetime DATETIME,
    topic_name TEXT,
    username TEXT
)";
$db->exec($queryTopic);

$queryMessages = 'CREATE TABLE IF NOT EXISTS messages (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    datetime DATETIME NOT NULL,
    username TEXT NOT NULL,
    message TEXT NOT NULL,
    topic_id INTEGER NOT NULL
)';
$db->exec($queryMessages);

$queryFeedback = 'CREATE TABLE IF NOT EXISTS message_feedback (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    user_id INTEGER NOT NULL,
    message_id INTEGER NOT NULL,
    feedback_type TEXT NOT NULL,
    date DATETIME NOT NULL,
    FOREIGN KEY (user_id) REFERENCES user (id),
    FOREIGN KEY (message_id) REFERENCES messages (id)
)';
$db->exec($queryFeedback);

$queryPinnedMessages = "CREATE TABLE IF NOT EXISTS pinned_messages (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    topic_id INTEGER NOT NULL,
    FOREIGN KEY (topic_id) REFERENCES topics (id)
)";
$db->exec($queryPinnedMessages);

$queryUserArea = "CREATE TABLE IF NOT EXISTS user_area (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    user_id INTEGER NOT NULL,
    topic_id INTEGER NOT NULL,
    FOREIGN KEY (user_id) REFERENCES user (id),
    FOREIGN KEY (topic_id) REFERENCES topics (id)
)";
$db->exec($queryUserArea);

$queryCreatePurgatory = "CREATE TABLE IF NOT EXISTS purgatory (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    username TEXT NOT NULL,
    reason TEXT NOT NULL
)";
$db->exec($queryCreatePurgatory);

// Close the database connection
$db->close();

//$error_message = "Database and tables created successfully.";


// Function to clear and validate user input ///////////////////////////////////////////////////////////////////////////////////////////////

function cleanInput($input) {
    $input = trim($input);
    $input = stripslashes($input);
    $input = htmlspecialchars($input);
    $input = urlencode($input);
    return $input;
}


// REGISTER /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

$dbFile = 'master.db';
if (!file_exists($dbFile)) {
    die("Database not found.");
}

$db = new SQLite3($dbFile);

if ($_SERVER["REQUEST_METHOD"] === "POST") {
    if (isset($_POST["login"])) {
    
// Process login
        $username = cleanInput($_POST["username"]);
        $password = cleanInput($_POST["password"]);

// Query the database to verify username and password
        $query = $db->prepare("SELECT id, username, hash_password, historic, status FROM user WHERE username = :username");
        $query->bindValue(':username', $username, SQLITE3_TEXT);
        $result = $query->execute();

        if ($row = $result->fetchArray(SQLITE3_ASSOC)) {
            if ($row['status'] === 'banned') {
// Get the reason for the user's ban
                $queryReason = $db->prepare("SELECT reason FROM purgatory WHERE username = :username");
                $queryReason->bindValue(':username', $username, SQLITE3_TEXT);
                $resultReason = $queryReason->execute();
                $banReason = $resultReason->fetchArray(SQLITE3_ASSOC);
                header("Location: purgatory.php?reason=" . urlencode($banReason['reason']));
                exit();
            } elseif (password_verify($password, $row['hash_password'])) {
// Check password
//            if (password_verify($password, $row['hash_password'])) {
                $_SESSION['user_id'] = $row['id'];
                $_SESSION['username'] = $row['username'];
                if ($row && isset($row["status"]) && isset($row["status"])) {
                    $_SESSION['status'] = $row['status'];
                }

// Add login date and time to "historic" column
                $loginDateTime = date('Y-m-d H:i:s');
                $historic = $row['historic'] . ',' . $loginDateTime;

// Update the "historic" column in the database
                $updateQuery = $db->prepare("UPDATE user SET historic = :historic WHERE id = :id");
                $updateQuery->bindValue(':historic', $historic, SQLITE3_TEXT);
                $updateQuery->bindValue(':id', $row['id'], SQLITE3_INTEGER);
                $updateQuery->execute();

//                $error_message = "Login successful.";
// Redirect to success page after login
                header("Location: index.php");
                exit();
            } else {
//                $error_message = "Incorrect password. Try again.";
                header("Location: index.php");
                exit();
            }
        } else {
//            $error_message = "Username not found. Please try again or register.";
            header("Location: index.php");
            exit();
        }
    } elseif (isset($_POST["register"])) {
        $username = cleanInput($_POST["username"]);
        $password = cleanInput($_POST["password"]);


// Define a list of restricted usernames
        $restrictedUsernames = ["admin", "administrador", "administrator", "adm", "bot", "zeyerq", "boss", "lordy", "script"];

// Check if the provided username is in the restricted list
        if (in_array(strtolower($username), $restrictedUsernames)) {
            $error_message .= "Username is restricted and cannot be registered.";
            header("Location: index.php");
            exit();
        } else {
// Query the database to check if the username is already registered
            $query_check = $db->prepare("SELECT id, date FROM user WHERE username = :username");
            $query_check->bindValue(':username', $username, SQLITE3_TEXT);
            $result_check = $query_check->execute();
            $row_check = $result_check->fetchArray(SQLITE3_ASSOC);

            if ($row_check) {
// Already registered user
// Try to log in automatically
                $query = $db->prepare("SELECT id, username, hash_password, status FROM user WHERE username = :username");
                $query->bindValue(':username', $username, SQLITE3_TEXT);
                $result = $query->execute();
                $row = $result->fetchArray(SQLITE3_ASSOC);

                if ($row) {
// Check password
                    if (password_verify($password, $row['hash_password'])) {
                        $_SESSION['user_id'] = $row['id'];
                        $_SESSION['username'] = $row['username'];
//                        $error_message .= " Login successful.";
                    } else {
//                        $error_message .= "Incorrect password.";
                        header("Location: index.php");
                        exit();
                    }
                } else {
//                    $error_message .= "Username not found.";
                    header("Location: index.php");
                    exit();
                }
            } else {
// Password hash
                $hashedPassword = password_hash($password, PASSWORD_DEFAULT);

// Insert the user into the database with "tourist" status
                $query = $db->prepare("INSERT INTO user (username, hash_password, date, status) VALUES (:username, :hash_password, :date, :status)");
                $query->bindValue(':username', $username, SQLITE3_TEXT);
                $query->bindValue(':hash_password', $hashedPassword, SQLITE3_TEXT);
                $query->bindValue(':date', date('Y-m-d H:i:s'), SQLITE3_TEXT);
                $query->bindValue(':status', "tourist", SQLITE3_TEXT);
                $query->execute();

// Log in automatically
                $_SESSION['username'] = $username;

                $query = $db->prepare("SELECT id, username, hash_password, historic, status FROM user WHERE username = :username");
                $query->bindValue(':username', $username, SQLITE3_TEXT);
                $result = $query->execute();

                if ($row = $result->fetchArray(SQLITE3_ASSOC)) {
                    if ($row && isset($row["status"]) && isset($row["status"])) {
                        $_SESSION['status'] = $row['status'];
                    }
                }

//            $error_message = "User successfully registered and logged in.";
            }
        }
    } elseif (isset($_POST["logout"])) {
// Process logout
        session_unset();
        session_destroy();
        header("Location: index.php");
        exit();
    }
}

// Close the database connection
$db->close();


// CLEAR SESSION /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

if ($_SERVER["REQUEST_METHOD"] === "POST" && isset($_POST["cleanse"])) {
    session_unset();
    session_destroy();
    header("Location: index.php");
    exit();
}


// ADD TOPIC /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

if (!isset($_SESSION["username"]) || $_SESSION["username"] === "") {
    $error_message = "You must be logged in to add a topic.";
} else {
    if ($_SERVER["REQUEST_METHOD"] === "POST") {
        if (isset($_POST["add_topic"])) {
// Use the cleanInput function to filter the topic input
            $topic_name = cleanInput($_POST["topic_name"]);
            $username = $_SESSION["username"];

// Connect to the database
            $db = new SQLite3('master.db');

// Insert the topic into the topic table
            $insertTopicQuery = $db->prepare("INSERT INTO topics (datetime, topic_name, username) VALUES (:datetime, :topic_name, :username)");
            $insertTopicQuery->bindValue(':datetime', date('Y-m-d H:i:s'), SQLITE3_TEXT);
            $insertTopicQuery->bindValue(':topic_name', $topic_name, SQLITE3_TEXT);
            $insertTopicQuery->bindValue(':username', $username, SQLITE3_TEXT);
            $result = $insertTopicQuery->execute();

            if ($result) {
//                $error_message = "Topic added successfully.";
            } else {
                $error_message = "An error occurred while adding the topic.";
            }

// Close the database connection
            $db->close();
        }
    }
}


// REMOVE TOPIC /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

if ($_SERVER["REQUEST_METHOD"] === "POST" && isset($_POST["delete_topic"])) {
// Connect to the database
    $db = new SQLite3('master.db');

// Get the ID of the topic to be deleted
    $topicId = isset($_POST["topic_id"]) ? intval($_POST["topic_id"]) : 0;

// Add logic to check user permissions (e.g. creator, admin, or boss)

// Begin a database transaction to ensure atomic operations
    $db->exec('BEGIN');

// Delete the messages associated with the topic
    $deleteMessagesQuery = $db->prepare("DELETE FROM messages WHERE topic_id = :topic_id");
    $deleteMessagesQuery->bindValue(':topic_id', $topicId, SQLITE3_INTEGER);
    $deleteMessagesQuery->execute();

// Delete the topic
    $deleteTopicQuery = $db->prepare("DELETE FROM topics WHERE id = :topic_id");
    $deleteTopicQuery->bindValue(':topic_id', $topicId, SQLITE3_INTEGER);
    $deleteTopicQuery->execute();

// Commit the transaction to save changes
    $db->exec('COMMIT');

// Close the connection to the database
    $db->close();

// Redirect the user back to the topic page or any page you want.
    header("Location: index.php");
}


// DELETE EMPTY TOPICS /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Check if the button to delete empty topics has been clicked
if (isset($_POST['delete_empty_topics'])) {
// Connect to the database
    $db = new SQLite3('master.db');

// Query to get the IDs of empty topics
    $queryEmptyTopics = $db->prepare("SELECT id FROM topics WHERE id NOT IN (SELECT topic_id FROM messages)");
    $resultEmptyTopics = $queryEmptyTopics->execute();

// Delete empty topics
    while ($rowEmptyTopic = $resultEmptyTopics->fetchArray(SQLITE3_ASSOC)) {
        $topicId = $rowEmptyTopic['id'];

// Query to delete topic from topic table
        $queryDeleteTopic = $db->prepare("DELETE FROM topics WHERE id = :topic_id");
        $queryDeleteTopic->bindValue(':topic_id', $topicId, SQLITE3_INTEGER);
        $queryDeleteTopic->execute();

// Query to delete messages associated with the topic from the message table
        $queryDeleteMessages = $db->prepare("DELETE FROM messages WHERE topic_id = :topic_id");
        $queryDeleteMessages->bindValue(':topic_id', $topicId, SQLITE3_INTEGER);
        $queryDeleteMessages->execute();
    }

// Close the database connection
    $db->close();
}


// PIN/UNPIN TOPIC /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

if (isset($_POST['pin_message'])) {
    $topicId = $_POST['topic_id'];
    $db = new SQLite3('master.db');

// Check if the topic is already pinned
    $queryCheckPinned = $db->prepare("SELECT id FROM pinned_messages WHERE topic_id = :topic_id");
    $queryCheckPinned->bindValue(':topic_id', $topicId, SQLITE3_INTEGER);
    $resultCheckPinned = $queryCheckPinned->execute();
    $existingPinned = $resultCheckPinned->fetchArray(SQLITE3_ASSOC);

    if ($existingPinned) {
// If the topic is already pinned, unpin it
        $queryUnpinMessage = $db->prepare("DELETE FROM pinned_messages WHERE id = :pinned_id");
        $queryUnpinMessage->bindValue(':pinned_id', $existingPinned['id'], SQLITE3_INTEGER);
        $queryUnpinMessage->execute();
    } else {
// If the topic is not already pinned, pin it
        $queryPinMessage = $db->prepare("INSERT INTO pinned_messages (topic_id) VALUES (:topic_id)");
        $queryPinMessage->bindValue(':topic_id', $topicId, SQLITE3_INTEGER);
        $queryPinMessage->execute();
    }

    $db->close();
    header("Location: index.php");
}


// USER AREA /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Function to PIN/UNPIN TOPIC
function isTopicPinnedByUser($username, $topic_id) {
    $db = new SQLite3('master.db');
    $query = $db->prepare("SELECT id FROM user_area WHERE user_id = (SELECT id FROM user WHERE username = :username) AND topic_id = :topic_id");
    $query->bindValue(':username', $username, SQLITE3_TEXT);
    $query->bindValue(':topic_id', $topic_id, SQLITE3_INTEGER);
    $result = $query->execute();
    $isPinned = $result->fetchArray(SQLITE3_ASSOC);
    $db->close();
    return $isPinned !== false;
}

// Pin a topic to the current user
function pinTopic($username, $topic_id) {
    $db = new SQLite3('master.db');
    $query = $db->prepare("INSERT INTO user_area (user_id, topic_id) VALUES ((SELECT id FROM user WHERE username = :username), :topic_id)");
    $query->bindValue(':username', $username, SQLITE3_TEXT);
    $query->bindValue(':topic_id', $topic_id, SQLITE3_INTEGER);
    $query->execute();
    $db->close();
}

// Unpin a topic for a specific user
function unpinTopic($username, $topic_id) {
    $db = new SQLite3('master.db');
    $query = $db->prepare("DELETE FROM user_area WHERE user_id = (SELECT id FROM user WHERE username = :username) AND topic_id = :topic_id");
    $query->bindValue(':username', $username, SQLITE3_TEXT);
    $query->bindValue(':topic_id', $topic_id, SQLITE3_INTEGER);
    $query->execute();
    $db->close();
}

// Logic for pinning topics
if (isset($_POST['pin_topic'])) {
    $topic_id = $_POST['topic_id'];
    $username = $_SESSION["username"];
    pinTopic($username, $topic_id);
    header("Location: index.php");
}

// Logic for unpinning topics
if (isset($_POST['unpin_topic'])) {
    $topic_id = $_POST['topic_id'];
    $username = $_SESSION["username"];
    unpinTopic($username, $topic_id);
    header("Location: index.php");
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>SimpleBR Forum</title>
    <link rel="stylesheet" type="text/css" href="styles.css">
</head>

<body>
    <div id="top"></div>
    <div class="forum-global">
        <div class="report-box">
            <form action="index.php" method="post">
                <input type="text" name="username" id="username" maxlength="20" required placeholder="Enter a username (maxlength=20)">
                <input type="password" name="password" id="password" maxlength="25" required placeholder="Enter a password (maxlength=25)">
                <input type="submit" name="login" value="Login">
                <input type="submit" name="register" value="Register">
                <input type="submit" name="logout" value="Logout">
            </form>
        </div>

        <br>
        <a href="index.php">
            <img src="logo.jpg" alt="Ícone" width="60" height="60" style="display: inline-block;">
            <div class="forum-title"><span>SimpleBR </span><span style="font-weight: 100;">Forum</span></div>
        </a>
        <br>

        <?php

        $dbFile = 'master.db';
        if (!file_exists($dbFile)) {
            die("Database not found.");
        }
        $db = new SQLite3($dbFile);
        $_SESSION['username'] = $username;
        $query = $db->prepare("SELECT id, username, hash_password, historic, status FROM user WHERE username = :username");
        $query->bindValue(':username', $username, SQLITE3_TEXT);
        $result = $query->execute();
        if ($row = $result->fetchArray(SQLITE3_ASSOC)) {
            if ($row && isset($row["status"]) && isset($row["status"])) {
                $_SESSION['status'] = $row['status'];
                $status = $row['status'];
            }
        }

        if (isset($_SESSION["username"]) && $_SESSION["username"] !== "" && !empty($_SESSION["status"]) && $status !== NULL ) {
// Database file name
            $dbFile = 'master.db';
            $db = new SQLite3($dbFile);
// Fetch the user's status in the database
            $username = $_SESSION["username"];
            $query = "SELECT date, status, reward FROM user WHERE username = :username";
            $stmt = $db->prepare($query);
            $stmt->bindValue(':username', $username, SQLITE3_TEXT);
            $result = $stmt->execute();
            $row = $result->fetchArray(SQLITE3_ASSOC);

            echo '<div id="info"></info>';
                echo '<span style="text-align: right;">';
                    echo '<div id="blue" style="text-align: right; font-size: 22px;">Hello, ' . htmlspecialchars(urldecode($_SESSION["username"]), ENT_QUOTES);

                    if (isset($_SESSION["username"]) && $_SESSION["username"] !== "") { 
                        if (!empty($_SESSION["status"])) {
                            if ($row && isset($row["status"]) && isset($row["status"])) {
                        '</span>';
                        echo '<div class="info_content">';
                            echo '<span style="color: #ffffff;">' . 'Status: ' . $row["status"] . '</span><br>';
                            if ($row && isset($row["date"])) {
                                $datetime = $row["date"];
                                $date = substr($datetime, 2, 8);
                                $hour = substr($datetime, 11);
                                echo '<span style="color: #ffffff;">' . 'Reg date: ' . $date . '</span><br>';
                                echo '<span style="color: #ffffff;">' . 'Reg hour: ' . $hour . '</span><br>';
                            }
                            if ($row && isset($row["reward"])) {
                                $reward = $row["reward"];
                                echo '<span style="color: #ffffff;">' . 'Reward: ' . $reward . '</span>';
                            }
                        echo '</div>';
                    echo '</div>';
                            }
                        }
                    }

// Close the database connection
            $db->close();
            echo '</div>';
        } else {
            echo '<div id="blue" style="text-align: right; font-size: 22px;">User not logged in</div>';
        }
        echo '<br>';
        ?>

        <?php
        if (!empty($error_message)) {
            echo "<p style='color: red; font-size: 20px;'>$error_message</p>";
        }
        ?>

        <?php
        $showBitcoinAddress = false;
        if (isset($_POST["donation"])) {
            $bitcoinAddress = "82TmqctTF4GNHNeYAA42h2HpqzZMCUtm18reFbYKkmDf1ASyG3i17fWc8PEdqHTwTBQntLW2VRKjjYAqUF6YHbzPUyd7Gev";
            $showBitcoinAddress = true;
        }
        if (isset($_POST["back"])) {
            $showBitcoinAddress = false;
        }
        ?>

        <?php if (!$showBitcoinAddress): ?>
            <form method="post" action="index.php">
                <input type="submit" name="donation" value="Donation xmr" class="custom-button">
            </form>
        <?php else: ?>
            <form method="post" action="index.php">
                <input type="submit" name="back" value="Voltar" class="custom-button">
            </form>
            <p><span style="color: #0EAEF8;">XMR wallet address:</span> <span style="color: #ffffff;"><?php echo $bitcoinAddress; ?></span></p>
        <?php endif; ?>

        <form action="index.php" method="post">
            <?php
            if (isset($_SESSION["username"]) && $_SESSION["username"] !== "" && !empty($_SESSION["status"])) {
                if ($_SESSION["status"] === "admin" || $_SESSION["status"] === "boss") {
                    echo '<input type="submit" name="delete_empty_topics" value="Delete Empty Topics">';
                }
            }
            ?>
        </form>

        <div class="forum_container">
            <div class="topics">
                <div id="separator">
                    <form action="index.php" method="post" style="flex: 1; display: flex;">
                        <input type="topic" name="topic_name" id="topic_name" maxlength="35" required placeholder="Enter topic name (maxlength=35)">
                        <input type="submit" name="add_topic" value="Add Topic">
                    </form>
                </div>

                <?php
// Connect to the database
                $db = new SQLite3('master.db');
// Query to get all topics, with user-pinned topics first
                $queryTopics = $db->prepare("SELECT t.id, t.topic_name, t.datetime, t.username, pm.id AS pinned
                                            FROM topics AS t
                                            LEFT JOIN pinned_messages AS pm ON t.id = pm.topic_id
                                            LEFT JOIN user_area AS ua ON t.id = ua.topic_id AND ua.user_id = (SELECT id FROM user WHERE username = :username)
                                            ORDER BY CASE
                                                        WHEN ua.topic_id IS NOT NULL THEN 1  -- Prioritizes user-pinned topics
                                                        ELSE 2  -- Topics not pinned by the user
                                                    END, pinned DESC, t.datetime DESC");
                $queryTopics->bindValue(':username', $_SESSION['username'], SQLITE3_TEXT);
                $result = $queryTopics->execute();

                $topics = [];
                while ($row = $result->fetchArray(SQLITE3_ASSOC)) {
                    $topics[] = $row;
                }

// Close the database connection
                $db->close();
                ?>

                <?php
                foreach ($topics as $topic):
// Check if the current topic is pinned
                    $isPinned = $topic['pinned'] !== null;

// Define the button color based on whether the topic is pinned
                    $buttonColor = $isPinned ? '#53E125' : '#FFF87A';
                    $buttonColorUser = $isPinned ? '#53E125' : 'transparent';
                    $pin_unpin = $isPinned ? 'PINNED' : 'PIN';
                    $backgroundColor = $isPinned ? '#252525' : '#000000';
                    $pin_unpin_user = $isPinned ? 'PINNED' : '';
                    ?>

                    <div class="topic_block" style="background-color: <?php echo $backgroundColor; ?>;">
                        <?php
                        $redirectUrl = 'messages.php?id=' . $topic['id'];
                        echo '<a href="' . $redirectUrl . '">';
                        ?>

                        <div class="topic_title">Topic:
                            <?php echo htmlspecialchars_decode(urldecode($topic['topic_name']), ENT_QUOTES); ?>

                            <?php
                            if (isset($_SESSION["username"]) && $_SESSION["username"] !== "") {
                                $db = new SQLite3('master.db');
// Query the database to get the user's status based on their username (assuming $_SESSION['username'] contains the username)
                                $queryStatus = $db->prepare("SELECT status FROM user WHERE username = :username");
                                $queryStatus->bindValue(':username', $_SESSION['username'], SQLITE3_TEXT);
                                $resultStatus = $queryStatus->execute();
                                if ($rowStatus = $resultStatus->fetchArray(SQLITE3_ASSOC)) {
                                    $userStatus = $rowStatus['status'];
                                }
                                $db->close();
                            }

// PIN/UNPIN button //////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                            if (isset($_SESSION["username"]) && $_SESSION["username"] !== "") {
                                if ($userStatus === "admin" || $userStatus === "boss") {
// Display a "Pin" button for each message
                                    echo '<form method="post" action="index.php">';
                                    echo '<input type="hidden" name="topic_id" value="' . $topic['id'] . '">';
                                    echo '<button type="submit" name="pin_message" style="margin-top: 0px; float: right; color: #000000; background-color: ' . $buttonColor . '; border: 0.5px solid ' . $buttonColor . '; font-size: 14px; font-weight: bold;">'. $pin_unpin .'</button>';
                                    echo '</form>';
                                } else {
// Topic fixed by administrators
                                    echo '<button type="submit" name="pin_message" style="margin-top: 0px; float: right; color: #000000; background-color: ' . $buttonColorUser . '; border: 0.5px solid ' . $buttonColorUser . '; font-size: 14px; font-weight: bold;" disabled>' . $pin_unpin_user . '</button>';

                                    $topic_id = $topic['id'];
                                    $isPinnedByUser = isTopicPinnedByUser($_SESSION["username"], $topic_id); // Checks if the user pinned this topic
// Display a "Pin" button for each message for non-admin users
                                    echo '<form method="post" action="index.php">';
                                    echo '<input type="hidden" name="topic_id" value="' . $topic['id'] . '">';
                                    if ($isPinnedByUser) {
                                        echo '<button type="submit" name="unpin_topic" style="margin-top: 0px; float: right; color: #000000; background-color: #FFF87A; border: 0.5px solid #FFF87A; font-size: 14px; font-weight: bold;">Unpin</button>';
                                    } else {
                                        echo '<button type="submit" name="pin_topic" style="margin-top: 0px; float: right; color: #000000; background-color: #53E125; border: 0.5px solid #53E125; font-size: 14px; font-weight: bold;">Pin</button>';
                                    }
                                    echo '</form>';
                                }
                            }

// Check if the current user has permission to delete the topic
                            if (isset($_SESSION["username"]) && $_SESSION["username"] !== "") {
                                if ($_SESSION["username"] === $topic['username'] || $userStatus === "admin" || $userStatus === "boss") {
                                    echo '<form method="post" action="index.php">';
                                    echo '<input type="hidden" name="topic_id" value="' . $topic['id'] . '">';
                                    echo '<button type="submit" name="delete_topic" style="margin-top: 0px; float: right; color: #000000; border: 0.5px solid red; background-color: red; font-size: 14px; font-weight: bold;">Delete</button>';
                                    echo '</form>';
                                }
                            }
                            ?>
                        </div>

                        <div class="topic_message">
                            <?php
// Connect to the database
                            $db = new SQLite3('master.db');

// Query to get the count of messages for this topic
                            $queryMessageCount = $db->prepare("SELECT COUNT(*) as message_count FROM messages WHERE topic_id = :topic_id");
                            $queryMessageCount->bindValue(':topic_id', $topic['id'], SQLITE3_INTEGER);
                            $messageCount = $queryMessageCount->execute()->fetchArray(SQLITE3_ASSOC);

// Close the database connection
                            $db->close();
                            ?>
                            Messages: <?php echo $messageCount['message_count']; ?>
                        </div>

                        <div class="topic_info">
                            Created by <?php echo htmlspecialchars(urldecode($topic['username']), ENT_QUOTES, 'UTF-8'); ?>,
                            <span class="topic_date">
                                <?php
                                $messageTime = strtotime($topic['datetime']);
                                $currentTime = time();
                                $timeElapsed = $currentTime - $messageTime;

                                if ($timeElapsed < 60) {
                                    $timeAgo = $timeElapsed . ($timeElapsed == 1 ? ' second' : ' seconds');
                                } elseif ($timeElapsed < 3600) {
                                    $minutes = floor($timeElapsed / 60);
                                    $timeAgo = $minutes . ($minutes == 1 ? ' minute' : ' minutes');
                                } elseif ($timeElapsed < 86400) {
                                    $hours = floor($timeElapsed / 3600);
                                    $timeAgo = $hours . ($hours == 1 ? ' hour' : ' hours');
                                } elseif ($timeElapsed < 604800) {
                                    $days = floor($timeElapsed / 86400);
                                    $timeAgo = $days . ($days == 1 ? ' day' : ' days');
                                } elseif ($timeElapsed < 2419200) {
                                    $weeks = floor($timeElapsed / 604800);
                                    $timeAgo = $weeks . ($weeks == 1 ? ' week' : ' weeks');
                                } else {
                                    $months = floor($timeElapsed / 2419200);
                                    $timeAgo = $months . ($months == 1 ? ' month' : ' months');
                                }
                                echo $timeAgo;
                                ?> ago
                            </span>
                        </div>
                    </div>
                    </a>
                <?php endforeach; ?>
            </div>

            <div class="description">
                <div class="enter_description">
                    <p style="text-align: left;" id="blue">Respect your privacy: do not accept JavaScript!</p>
                    <?php
// Connect to the database
                    $db = new SQLite3($dbFile);
// Query to obtain the total number of messages sent
                    $queryTotalMessages = "SELECT COUNT(*) as message_count FROM messages";
                    $resultMessages = $db->query($queryTotalMessages);
                    $messageCount = $resultMessages->fetchArray(SQLITE3_ASSOC);
// Query to obtain the total number of topics created
                    $queryTotalTopics = "SELECT COUNT(*) as topic_count FROM topics";
                    $resultTopics = $db->query($queryTotalTopics);
                    $topicCount = $resultTopics->fetchArray(SQLITE3_ASSOC);
// Query to get the total number of users
                    $queryTotalUsers = "SELECT COUNT(*) as user_count FROM user";
                    $resultUsers = $db->query($queryTotalUsers);
                    $userCount = $resultUsers->fetchArray(SQLITE3_ASSOC);
// Close the database connection
                    $db->close();
// Display the results
                    echo "Messages: " . $messageCount['message_count'] . "<br>";
                    echo "Topics: " . $topicCount['topic_count'] . "<br>";
                    echo "Users: " . $userCount['user_count'] . "<br>";
                    ?>
                </div>
                <div class="division"></div>
                <div class="enter_description">
                    <div id="add">
                        <?php
                        echo '<form action="index.php" method="post">';
                            echo '<button type="submit" name="cleanse">Clear session</button>';
                        echo '</form>';
                        ?>
                        <a href="#top" id="footer_section">Up</a><br>
                        <a href="" id="footer_section">Reload</a><br>
                        <a href="https://gitea.com/log03202reset/SimpleBRForum/src/branch/main/" id="footer_section">Source Code link</a>
                    </div>
                    <br>

                    <div id="license">
                        <div id="warning">Copyright (c) 2023 zeyerq</div>
                        <div class="copyright">
                            GNU General Public License (GPL)<br>
                            Copyright (c) 2023 zeyerq<br><br>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
